/*
    SPDX-FileCopyrightText: 2009 Volker Krause <vkrause@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef AKONADICONSOLE_DBACCESS_H
#define AKONADICONSOLE_DBACCESS_H

class QSqlDatabase;

namespace DbAccess
{
QSqlDatabase database();
}

#endif
