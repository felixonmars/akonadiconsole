/*
    SPDX-FileCopyrightText: 2013 Daniel Vrátil <dvratil@redhat.com>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef AKONADICONSOLE_METATYPES_H
#define AKONADICONSOLE_METATYPES_H

#include <akonadi/private/notificationmessagev2_p.h>

#include <QMetaType>

Q_DECLARE_METATYPE(QList<QByteArray>)
Q_DECLARE_METATYPE(QList<long long>)

#endif
